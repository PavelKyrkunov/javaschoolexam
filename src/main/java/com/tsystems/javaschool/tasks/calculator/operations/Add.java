package com.tsystems.javaschool.tasks.calculator.operations;

import com.tsystems.javaschool.tasks.calculator.exceptions.OverflowException;

//operation of addition
public class Add extends Operation {
    public Add(double l, double r) {
        super(l, r);
    }

    @Override
    public double calculate() throws OverflowException {
        if (l + r > Integer.MAX_VALUE || l - r < Integer.MIN_VALUE) {
            throw new OverflowException();
        }
        return l + r;
    }
}
