package com.tsystems.javaschool.tasks.calculator.operations;

import com.tsystems.javaschool.tasks.calculator.exceptions.*;

//operation of division
public class Div extends Operation {
    public Div(double l, double r) {
        super(l, r);
    }

    @Override
    public double calculate() throws OverflowException, InvalidExpression {

        if (r == 0) {
            throw new InvalidExpression();
        } else if (l / r > Integer.MAX_VALUE || l / r < Integer.MIN_VALUE) {
            throw new OverflowException();
        }
        return l / r;
    }
}
