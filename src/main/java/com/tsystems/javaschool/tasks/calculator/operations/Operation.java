package com.tsystems.javaschool.tasks.calculator.operations;

import com.tsystems.javaschool.tasks.calculator.exceptions.*;

//abstract class for math operations
public abstract class Operation {
    double l;
    double r;

    Operation(double l, double r) {
        this.l = l;
        this.r = r;
    }

    public abstract double calculate() throws OverflowException, InvalidExpression;
}
