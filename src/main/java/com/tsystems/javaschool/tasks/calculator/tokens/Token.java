package com.tsystems.javaschool.tasks.calculator.tokens;

//class describing Token
public class Token {
    private TokenType type;
    private String value;
    private String status;

    Token(TokenType type, String value, String status) {
        this.type = type;
        this.value = value;
        this.status = status;
    }

    public TokenType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getStatus() {
        return status;
    }
}
