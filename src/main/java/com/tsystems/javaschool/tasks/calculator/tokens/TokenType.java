package com.tsystems.javaschool.tasks.calculator.tokens;

public enum TokenType {
    LEFT_BR, RIGHT_BR, PLUS, MINUS, MUL, DIV, CONST, END
}
