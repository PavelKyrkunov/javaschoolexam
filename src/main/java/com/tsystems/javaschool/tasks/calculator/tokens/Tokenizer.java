package com.tsystems.javaschool.tasks.calculator.tokens;

import com.tsystems.javaschool.tasks.calculator.util.ErrorChecking;
import com.tsystems.javaschool.tasks.calculator.exceptions.InvalidExpression;

import java.util.ArrayList;
import java.util.List;

public class Tokenizer {
    private List<Token> tokens = new ArrayList<>();
    private String expr;

    public Tokenizer(String expr) {
        this.expr = expr;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    /**
     * start tokenizer
     * @throws InvalidExpression
     */
    public void start_tokenize() throws InvalidExpression {
        //add token of the beginning of expression
        tokens.add(new Token(TokenType.END, "beginning of expression", "un"));
        tokenize(expr);
        //add token of the end ot expression
        tokens.add(new Token(TokenType.END, "end of expression", "un"));
    }

    /**
     * convert input statement into list of tokens
     * @param s input statement
     * @throws InvalidExpression
     */
    private void tokenize(String s) throws InvalidExpression {
        for (int i = 0; i < s.length(); i++) {
            //skip spaces
            if (Character.isWhitespace(s.charAt(i))) {
                continue;
            }
            switch (s.charAt(i)) {
                case '(':
                    tokens.add(new Token(TokenType.LEFT_BR, "(", "bracket"));
                    break;

                case ')':
                    tokens.add(new Token(TokenType.RIGHT_BR, ")", "bracket"));
                    break;

                case '+':
                    tokens.add(new Token(TokenType.PLUS, "+", "operation"));
                    break;

                case '-':

                    tokens.add(new Token(TokenType.MINUS, "-", "operation"));
                    break;

                case '*':
                    tokens.add(new Token(TokenType.MUL, "*", "operation"));
                    break;

                case '/':
                    tokens.add(new Token(TokenType.DIV, "/", "operation"));
                    break;

                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    int j = i;
                    //get numbers from statement as CONST tokens
                    while (j < s.length() && (Character.isDigit(s.charAt(j)) || s.charAt(j) == '.')) {
                        //check multiple point
                        if (s.charAt(j) == '.' && s.charAt(j + 1) == '.') {
                            throw new InvalidExpression();
                        }
                        j++;
                    }
                    String number = s.substring(i, j);
                    tokens.add(new Token(TokenType.CONST, number, "operand"));
                    i = j - 1;
                    break;
                    //found unknown symbol
                default:
                    throw new InvalidExpression();
            }
        }

        ErrorChecking check = new ErrorChecking();
        try {
            check.InvalidExpression(tokens);
        } catch (InvalidExpression invalidExpr) {
            throw invalidExpr;
        }
        //parenthesis check (amount of opening and closing brackets)
        if (!check.brackets(tokens)) {
            throw new InvalidExpression();
        }
    }
}
