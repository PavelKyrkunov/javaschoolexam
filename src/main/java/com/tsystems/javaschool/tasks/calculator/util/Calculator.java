package com.tsystems.javaschool.tasks.calculator.util;

import com.tsystems.javaschool.tasks.calculator.exceptions.*;
import com.tsystems.javaschool.tasks.calculator.operations.*;
import com.tsystems.javaschool.tasks.calculator.tokens.Tokenizer;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

public class Calculator {
    private Stack<Double> stack = new Stack<>();
    private List<String> list;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        double answer;

        if (isBlank(statement)) {
            return null;
        }

        Tokenizer token = new Tokenizer(statement);
        try {
            token.start_tokenize();
        } catch (InvalidExpression e) {
            return null;
        }
        RPN rpn = new RPN();

        try {
            list = rpn.poland(token.getTokens());
        } catch (InvalidExpression e) {
            return null;
        }

        try {
            answer = answer();
        } catch (OverflowException | InvalidExpression e) {
            return null;
        }
        return (roundDigit(answer));

    }

    /**
     *
     * @return answer of calculation
     * @throws OverflowException
     * @throws InvalidExpression
     */
    private double answer() throws OverflowException, InvalidExpression {

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals("+")) {
                double r = stack.pop();
                double l = stack.pop();
                try {
                    stack.push(new Add(l, r).calculate());
                } catch (OverflowException e) {
                    throw e;
                }
            } else if (list.get(i).equals("-")) {
                double r = stack.pop();
                double l = stack.pop();
                try {
                    stack.push(new Minus(l, r).calculate());
                } catch (OverflowException e) {
                    throw e;
                }
            } else if (list.get(i).equals("*")) {
                double r = stack.pop();
                double l = stack.pop();
                try {
                    stack.push(new Multiply(l, r).calculate());
                } catch (OverflowException e) {
                    throw e;
                }
            } else if (list.get(i).equals("/")) {
                double r = stack.pop();
                double l = stack.pop();
                try {
                    stack.push(new Div(l, r).calculate());
                } catch (InvalidExpression | OverflowException e) {
                    throw e;
                }
            } else {
                stack.push(Double.valueOf(list.get(i)));
            }
        }
        return stack.pop();

    }

    /**
     *
     * @param str is string to be checked
     * @return true if statement is blank
     */
    private static boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((!Character.isWhitespace(str.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param digit is number to be rounded
     * @return answer rounded to 4 decimal places
     */
    private static String roundDigit(double digit) {

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.####", otherSymbols);
        df.setRoundingMode(RoundingMode.CEILING);

        return df.format(digit);
    }


}
