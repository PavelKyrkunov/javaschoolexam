package com.tsystems.javaschool.tasks.calculator.util;

import com.tsystems.javaschool.tasks.calculator.exceptions.InvalidExpression;
import com.tsystems.javaschool.tasks.calculator.tokens.*;

import java.util.List;

public class ErrorChecking {
    /**
     * check validity of statement(using tokens)
     * @param tokens
     * @throws InvalidExpression
     */
    public void InvalidExpression(List<Token> tokens) throws InvalidExpression {
        for (int k = 1; k < tokens.size() - 1; k++) {

            //if opening bracket before operation
            if (tokens.get(k).getType().equals(TokenType.LEFT_BR) && tokens.get(k + 1).getStatus().equals("operation")) {
                throw new InvalidExpression();
            }

            //if opening bracket after smt except operation, beginning of statement or another opening bracket
            if (tokens.get(k).getType().equals(TokenType.LEFT_BR) && (!tokens.get(k - 1).getStatus().equals("operation")) &&
                    (!tokens.get(k - 1).getType().equals(TokenType.END)) && (!tokens.get(k - 1).getType().equals(TokenType.LEFT_BR))) {
                throw new InvalidExpression();
            }

            //if closing bracket before smt except operation, ending of statement or another closing bracket
            if (tokens.get(k).getType().equals(TokenType.RIGHT_BR) && (!tokens.get(k + 1).getStatus().equals("operation")) &&
                    (!tokens.get(k + 1).getType().equals(TokenType.END)) && (!tokens.get(k + 1).getType().equals(TokenType.RIGHT_BR))) {
                throw new InvalidExpression();
            }

            //if more than one operation in a row
            if (tokens.get(k).getStatus().equals("operation") && tokens.get(k + 1).getStatus().equals("operation")) {
                throw new InvalidExpression();
            }

        }

        //if last token is operation or opening bracket
        if (tokens.get(tokens.size() - 1).getStatus().equals("operation") || tokens.get(tokens.size() - 1).getType().equals(TokenType.LEFT_BR)) {
            throw new InvalidExpression();
        }
    }

    /**
     * parenthesis check (amount of opening and closing brackets)
     * @param tokens
     * @return true if amount of brackets is ok
     */
    public boolean brackets(List<Token> tokens) {
        int leftbr = 0;
        int rightbr = 0;
        for (int k = 0; k < tokens.size(); k++) {
            if (tokens.get(k).getType().equals(TokenType.LEFT_BR)) {
                leftbr++;
            }
            if (tokens.get(k).getType().equals(TokenType.RIGHT_BR)) {
                rightbr++;
            }
        }
        if (leftbr == rightbr) {
            return true;
        } else {
            return false;
        }
    }
}

