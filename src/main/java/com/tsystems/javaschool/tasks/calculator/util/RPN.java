package com.tsystems.javaschool.tasks.calculator.util;

import com.tsystems.javaschool.tasks.calculator.exceptions.InvalidExpression;
import com.tsystems.javaschool.tasks.calculator.tokens.*;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;

class RPN {
    private Stack<Token> stack = new Stack<>();
    private List<String> polandTokens = new ArrayList<>();

    /**
     * convert statement to RPN
     * @param tokensNat is list of tokens in infix record
     * @return list of tokens in RPN
     * @throws InvalidExpression
     */
    List<String> poland(List<Token> tokensNat) throws InvalidExpression {


        for (int i = 1; i < tokensNat.size(); i++)
            switch (String.valueOf(tokensNat.get(i).getType())) {
                case "CONST":
                    polandTokens.add(tokensNat.get(i).getValue());
                    break;

                case "PLUS":
                case "MINUS":
                    try {
                        while (stack.peek().getType() == TokenType.PLUS || stack.peek().getType() == TokenType.DIV || stack.peek().getType() == TokenType.MUL
                                || stack.peek().getType() == TokenType.MINUS) {
                            polandTokens.add(stack.pop().getValue());
                        }
                    } catch (EmptyStackException e) {
                    } finally {
                        stack.push(tokensNat.get(i));
                    }
                    break;

                case "MUL":
                case "DIV":
                    try {
                        while (stack.peek().getType() == TokenType.DIV || stack.peek().getType() == TokenType.MUL) {
                            polandTokens.add(stack.pop().getValue());
                        }
                    } catch (EmptyStackException e) {
                    } finally {
                        stack.push(tokensNat.get(i));
                    }
                    break;

                case "LEFT_BR":
                    stack.push(tokensNat.get(i));
                    break;

                case "RIGHT_BR":
                    try {
                        while (stack.peek().getType() != TokenType.LEFT_BR) {
                            polandTokens.add(stack.pop().getValue());
                        }
                        stack.pop();
                    } catch (EmptyStackException e) {
                    }
                    break;

                case "END":
                    while (!stack.isEmpty()) {
                        polandTokens.add(stack.pop().getValue());
                    }
                    break;

            }

        return polandTokens;

    }

}
