package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


class PyramidBuilder {
    /**
     * Build a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeroes.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given output
     */
    int[][] buildPyramid(List<Integer> inputNumbers) {

        //if inputNumbers list contains nulls, throw CannotBuildPyramidException
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        //get how many rows the pyramid have
        int rows = getRowNumber(inputNumbers);

        //if input list has no desired number of numbers to build
        //the pyramid throw CannotBuildPyramidException
        if (rows == -1) {
            throw new CannotBuildPyramidException();
        }
        //number of columns
        int cols = 2 * rows - 1;
        //2d array with result
        int[][] pyramid = new int[rows][cols];
        //sort input list
        Collections.sort(inputNumbers);
        //create queue with input numbers to work with it
        //more comfortable without indexes
        Queue<Integer> queue = new LinkedList<>(inputNumbers);
        //the first position of number in the row
        int leftPos = cols / 2;

        //filling array with numbers
        for (int i = 0; i < pyramid.length; i++) {
            int count = leftPos;

            for (int j = 0; j <= i; j++) {
                //write the smallest int in array
                pyramid[i][count] = queue.remove();
                //skip one cell
                count += 2;
            }
            leftPos--;
        }
        return pyramid;
    }

    /**
     * by using this equation,  n(n+1)/2, where n is the size of the list
     * we can calculate the root, which will be the row number, of n(n+1)/2
     * root = (-b + (sqrt(b^2 - 4ac))/2a (ax^2+bx+c),
     * in our case, for n(n+1)/2 = n^2+n-2, it will be  = (-1 + sqrth(1 + 8n))/2,
     * or ((sqrt(1 + 8n) -1) / 2;
     *
     * @param input list of Integers
     * @return row number in pyramid or -1 if number of integers is not suitable
     */
    private static int getRowNumber(List<Integer> input) {
        int listSize = input.size();
        double result = (Math.sqrt(1 + 8 * listSize) - 1) / 2;
        if (result == Math.ceil(result)) {
            return (int) result;
        }
        return -1;

    }
}

