package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return true if possible, otherwise false
     */
     <T> boolean find(List<T> x, List<T> y) {
        boolean flag = true;

        //if x or y sequences are null throw IllegalArgumentException
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }


        int k = 0;
        //iterating over both sequences and comparing elements
        for (int i = 0; i < x.size(); i++) {
            //if didnt find element from x in y, break
            if (!flag) {
                break;
            }
            flag = false;
            for (int j = k; j < y.size(); j++) {
                if (y.get(j).equals(x.get(i))) {
                    k = j + 1;
                    //if found element from x in y
                    flag = true;
                    break;

                }
            }
        }
        return flag;
    }
}
